import { NavLink } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import "./index.css"


function NavBar() {
  return (
    <Navbar variant="dark" bg="dark" expand="lg" style={{marginBottom: "6rem"}}>
    <Container fluid style={{justifyContent: "flex-start"}}>
      <Navbar.Brand>
        <NavLink to="/" 
        style={{textDecoration: "none", color: "white", width: "100%"}}>
          CarCar
        </NavLink>
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="navbar-dark-example" />
      <Navbar.Collapse id="navbar-dark-example">
        <Nav>
          <NavDropdown
            id="nav-dropdown-dark-example"
            title="Inventory"
            menuVariant="dark"
          >
            <NavDropdown.Item> 
              <NavLink to="inventory/manufacturer/create/" 
              style={{textDecoration: "none", color: "white"}}>
                Create a New Manufacturer
              </NavLink>
            </NavDropdown.Item>

            <NavDropdown.Item> 
              <NavLink to="inventory/model/create/" 
              style={{textDecoration: "none", color: "white"}}>
                Create a New Model
              </NavLink>
            </NavDropdown.Item>

            <NavDropdown.Item> 
              <NavLink to="inventory/automobile/create/" 
              style={{textDecoration: "none", color: "white"}}>
                Create a New Automobile
              </NavLink>
            </NavDropdown.Item>

            <NavDropdown.Divider />

            <NavDropdown.Item> 
              <NavLink to="inventory/manufacturers/" 
              style={{textDecoration: "none", color: "white"}}>
                List of Manufacturers
              </NavLink>
            </NavDropdown.Item>

            <NavDropdown.Item> 
              <NavLink to="inventory/models/" 
              style={{textDecoration: "none", color: "white"}}>
                List of Models
              </NavLink>
            </NavDropdown.Item>

            <NavDropdown.Item> 
              <NavLink to="inventory/automobiles/" 
              style={{textDecoration: "none", color: "white"}}>
                List of Automobiles
              </NavLink>
            </NavDropdown.Item>

          </NavDropdown>
        </Nav>
      </Navbar.Collapse>

      <Navbar.Collapse id="navbar-dark-example">
        <Nav>
          <NavDropdown
            id="nav-dropdown-dark-example"
            title="Sales Department"
            menuVariant="dark"
          >
            <NavDropdown.Item> 
              <NavLink to="sales/sales_person/create" 
              style={{textDecoration: "none", color: "white"}}>
                Create a New Sales Person
              </NavLink>
            </NavDropdown.Item>

            <NavDropdown.Item> 
              <NavLink to="sales/sales_customer/create/" 
              style={{textDecoration: "none", color: "white"}}>
                Create a New Sales Customer
              </NavLink>
            </NavDropdown.Item>

            <NavDropdown.Item> 
              <NavLink to="sales/create/" 
              style={{textDecoration: "none", color: "white", width: "100%"}}>
                Create a New Sale
              </NavLink>
            </NavDropdown.Item>

            <NavDropdown.Divider />

            <NavDropdown.Item> 
              <NavLink to="sales/list/" 
              style={{textDecoration: "none", color: "white"}}>
                List of Sales
              </NavLink>
            </NavDropdown.Item>

            <NavDropdown.Item> 
              <NavLink to="sales/sales_person_history/" 
              style={{textDecoration: "none", color: "white"}}>
                List of Sales Persons' History
              </NavLink>
            </NavDropdown.Item>

          </NavDropdown>
        </Nav>
      </Navbar.Collapse>

      <Navbar.Collapse id="navbar-dark-example">
        <Nav>
          <NavDropdown
            id="nav-dropdown-dark-example"
            title="Service Department"
            menuVariant="dark"
          >
            <NavDropdown.Item> 
              <NavLink to="services/technician/create/" 
              style={{textDecoration: "none", color: "white"}}>
                Create a New Service Technician
              </NavLink>
            </NavDropdown.Item>

            <NavDropdown.Item> 
              <NavLink to="services/customer/create/" 
              style={{textDecoration: "none", color: "white"}}>
                Create a New Service Customer
              </NavLink>
            </NavDropdown.Item>

            <NavDropdown.Item> 
              <NavLink to="services/appointment/create/" 
              style={{textDecoration: "none", color: "white"}}>
                Create a New Service Appointment
              </NavLink>
            </NavDropdown.Item>

            <NavDropdown.Divider />

            <NavDropdown.Item> 
              <NavLink to="services/list/" 
              style={{textDecoration: "none", color: "white"}}>
                List of Service Appointments
              </NavLink>
            </NavDropdown.Item>

            <NavDropdown.Item> 
              <NavLink to="services/automobile/history" 
              style={{textDecoration: "none", color: "white"}}>
                List of Appointments for Automobile
              </NavLink>
            </NavDropdown.Item>

          </NavDropdown>
        </Nav>
      </Navbar.Collapse>
    </Container>
  </Navbar>
  )
}

export default NavBar;
